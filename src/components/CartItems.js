import {Table, Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import {AiFillDelete} from  'react-icons/ai';

import {useState} from 'react';




export default function CartItems({cartProp}) {
  const{_id, productId, qty, subtotal } = cartProp;

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const navigate = useNavigate();

   fetch(`${process.env.REACT_APP_API_URL}/product/searchOne/${cartProp.productId}`)
  .then(result => result.json())
  .then(data => {
    if (data){
      setName(data[0].name);
      setDescription(data[0].description);
    
    }
    else {

    } 
  })

  function DeleteItem(productId){
      fetch(`${process.env.REACT_APP_API_URL}/order/removeItem`,{
      method: 'PUT',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body:JSON.stringify({
        _id : localStorage.getItem('orderId'),
        productId : productId
      })
    }).then(result=>result.json())
    .then(data => {
        
    })
   
  }
 

  return (
    
        <tr>
          <td>{name}</td>
          <td>{description}</td>
          <td>{subtotal}</td>
          <td>{qty}</td>
          <td>
              
                  <Link to ={'/cartview'}onClick={()=>DeleteItem(productId)}  className="text-dark"><AiFillDelete/></Link>
               
                  
          </td>
        </tr>
      
  );
}
