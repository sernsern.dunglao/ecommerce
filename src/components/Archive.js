import {Navigate, useParams} from "react-router-dom";
import {useContext, useEffect, useState} from 'react';

export default function Archive() {
	const [name,setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [imgLink, setImgLink] = useState('');
	
	
	const {productId} = useParams();
	const {status} = useParams();
	

	fetch(`${process.env.REACT_APP_API_URL}/product/retrieveOne/${productId}`,{
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
  })
  .then(result=> result.json())
  .then(data=> {
			fetch(`${process.env.REACT_APP_API_URL}/product/archiveProduct`,{
			  method : 'POST',
			  headers:{
			      'Content-Type': 'application/json',
			      Authorization: `Bearer ${localStorage.getItem('token')}`
			    },
			    body:JSON.stringify({
			      _id : data._id,
			      isActive : status
			    })
			})
	 })

return (
	<Navigate to = "/viewproducts"/>

	)

}
