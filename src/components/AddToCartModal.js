
import React, { useState, useEffect } from "react";
import { Modal, Button, div, Row, Col, Image, InputGroup, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import {Link, useNavigate} from 'react-router-dom'

const AddToCartModal = (props) => {
  // const [loginModalShow, setLoginModalShow] = useState(props.showModal);
  

  const [productId, setProductId] = useState();
  const [name,setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [imgLink, setImgLink] = useState('');
  const [qty, setQty] = useState('1');

  const navigate = useNavigate();

 
  
     if (props.prop){
       fetch(`${process.env.REACT_APP_API_URL}/product/searchOne/${props.prop}`)
      .then(result => result.json())
      .then(data => {
        if (data){
          setName(data[0].name);
          setDescription(data[0].description);
          setPrice(data[0].price);
          setImgLink(data[0].imgLink);
         
        }
        else {

        } 
      })
    }

   async function AddToCart (){

      
      //router.put("/addItem", auth.verify, orderController.addToCart);

      await fetch(`${process.env.REACT_APP_API_URL}/order/addItem`,{
              method: 'PUT',
              headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
              },
              body: JSON.stringify({
                  _id: `${localStorage.getItem('orderId')}`,
                  products: [{
                    productId: props.prop,
                    qty: qty
                  }]
                })
            }).then(result=>result.json())
            .then(data=>{
              navigate('/productsearch')

            })
    }

   
    
  

  return (
    
      <Modal
        show={props.show}
        onHide={props.close}
        size="lg"

        aria-labelledby="contained-modal-title-vcenter"
        centered
        className="shadow"
      >
        <Modal.Body className="d-flex">
          <div className="col-12 p-3" >
          <Row>
            <Col className="col-4 text-center">
              <Image style={{ width: '14.6rem' }} className="img-fluid" src={`${imgLink}`} />
            </Col>
              <Col>
                <Row>
                <h3>{name}</h3>
                <p> {description} </p>
                </Row>
                <Row className="d-flex align-items-center justify-content-center">
                  <Col className="col-6">
                    <h6>Price: {price}</h6>
                  </Col>
                  <Col className="col-6">
                    <InputGroup size="sm" className="mb-3">
                            <InputGroup.Text id="inputGroup-sizing-sm">QTY</InputGroup.Text>
                            <Form.Control
                              value={qty}
                              aria-label="Small"
                              aria-describedby="inputGroup-sizing-sm"
                              onChange ={event=> setQty(event.target.value)}
                            />
                          </InputGroup>
                  </Col>
                </Row>
                <Row className="mt-2 text-center">
                  <Col className="col-6">
                  <Button className="btn-secondary shadow" onClick={() => {
                        AddToCart();
                        props.close();
                      }}>Add to Cart</Button>
                  </Col>
                  <Col className="col-6">
                  <Button className="btn-secondary shadow" onClick={props.close}>Cancel</Button>
                  </Col>
                </Row>
              </Col>
            </Row>

          </div>

        </Modal.Body>
      </Modal>
    
  )
}

export default AddToCartModal;
