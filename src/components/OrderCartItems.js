import {Table, Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import {AiFillDelete} from  'react-icons/ai';

import {useState} from 'react';




export default function OrderCartItems({orderCartProp}) {
  const{_id, productId, qty, subtotal } = orderCartProp;

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const navigate = useNavigate();

   fetch(`${process.env.REACT_APP_API_URL}/product/searchOne/${orderCartProp.productId}`)
  .then(result => result.json())
  .then(data => {
    if (data){
      setName(data[0].name);
      setDescription(data[0].description);
    
    }
    else {

    } 
  })

  
 

  return (
    
        <tr>
          <td>{name}</td>
          <td>{description}</td>
          <td>{subtotal}</td>
          <td>{qty}</td>
          <td hidden>
              
                  
                  
          </td>
        </tr>
      
  );
}
