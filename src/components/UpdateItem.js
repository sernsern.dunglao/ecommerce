import {Form, Container, Button, Row, Col} from 'react-bootstrap';
import {Fragment, useState, useEffect,useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate, useParams, useNavigate, NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js';



export default function UpdateProduct() {
const [name,setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState('');
const [imgLink, setImgLink] = useState('');
const [isActive, setIsActive] = useState(false);

const {user, setUser} = useContext(UserContext);

const navigate = useNavigate();

const {productId} = useParams();

useEffect(()=>{

  fetch(`${process.env.REACT_APP_API_URL}/product/retrieveOne/${productId}`,{
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
  })
  .then(result=> result.json())
  .then(data=> {
    setName(data.name);
    setDescription(data.description);
    setPrice(data.price);
    setImgLink(data.imgLink);
  })
},[productId]);

  useEffect(() =>{
    
    if (name !== "" && description !== "" && price !== "" && imgLink !== "" && price >= 1){
      setIsActive(false);
    }
    else{
      setIsActive(true);
    }
  }, [name,description, price, imgLink])

function initiateUpdate(event){
  event.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/product/updateProduct`,{
    method : 'POST',
    headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body:JSON.stringify({
        _id : productId,
        name : name,
        description : description,
        price: price,
        imgLink : imgLink
      })
  })
  .then(result => result.json())
  .then(data => { 


      if (data) {

        Swal.fire({
            title: 'Item was successfully updated',
            text: `Name: ${name} \nDescription: ${description} \nPrice: ${price}`,
            imageUrl: `${imgLink}`,
            imageHeight: 200,
            imageWidth: 300,
            
        })
          setName('');
          setDescription('');
          setPrice('');
          setImgLink('');
          navigate("/viewproducts");
          
      }
      else{
        Swal.fire({
          title: "Item update unsuccessful.",
          icon: "error",
          text: "Please check the details or contact your administrator."

        })


      }

  })






}



  return (
  
    user && user.isAdmin? 
    
    <Fragment>
      <Container>
      <Form className="mb-3 mt-5" onSubmit ={event =>initiateUpdate(event)} autoComplete="off">
        <Form.Group>
          <Form.Label>Item Name</Form.Label>
          <Form.Control
            placeholder="Item Name"
            value ={name}
            aria-describedby="basic-addon1"
            className="mb-3 shadow"
            onChange ={event=> setName(event.target.value)}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Description</Form.Label>
          <Form.Control 
            as="textarea" aria-label="With textarea"
            placeholder="Tell us about the item"
            value ={description}
            aria-describedby="basic-addon2"
            className="mb-3 shadow"
            onChange ={event=> setDescription(event.target.value)}
          />
        </Form.Group>
          
          
        <Form.Group>
        <Row>
          <Col className=" col-6">
            <Form.Label>Price</Form.Label>
            <Form.Control
              placeholder="₱"
              value ={price}
              aria-describedby="basic-addon3"
              className="mb-3 shadow"
              onChange ={event=> setPrice(event.target.value)}
            />
        </Col>
        <Col className=" col-6">
            <Form.Label>Image Link</Form.Label>
            <Form.Control
              placeholder="www.drive.google.com"
              value ={imgLink}
              aria-describedby="basic-addon4"
              className="mb-3 shadow"
              onChange ={event=> setImgLink(event.target.value)}
            />
        </Col>
        </Row>
        </Form.Group>
        
        <Button variant="primary" type="submit" disabled={isActive} className="m-2 shadow">
          Update
        </Button>
        <Button as = {NavLink} to = "/viewproducts" className="m-2 shadow">
          Cancel
        </Button>

      </Form>
    </Container>
    </Fragment>
    :

    <Navigate to = "/*"/>

    
  );
}
