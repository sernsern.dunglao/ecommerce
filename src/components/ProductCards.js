import {Row, Col, Button, Card, Container} from 'react-bootstrap';
import {useState, useEffect, useContext, Fragment} from 'react';
import {NavLink} from 'react-router-dom'	

import AddToCartModal from './AddToCartModal.js'

import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom';


export default function ProductCards({searchProductProp}){
	const {user} = useContext(UserContext);
	
	const [showAddToCart, setShowAddToCart] = useState(false);
	const [productSearchId, setProductSearchId] = useState();

	const { _id, name, description, price, imgLink } = searchProductProp;
		

	return(
	<Fragment>
		<Row className = "mt-2">
			<Col className="">
				<Container>
				<Card style={{ width: '15rem', height: '27rem' }} className="shadow">
				      <Card.Img style={{ width: '14.6rem', height: '10rem' }} variant="top" src={`${imgLink}`} />
				      <Card.Body className="d-flex flex-column">
				        <Card.Title style={{ fontSize: '1rem' }}>{name}</Card.Title>
				        <Card.Text style={{ fontSize: '0.8rem' }}>{description}</Card.Text>
				        <Card.Text style={{ fontSize: '0.8rem' }} className="mt-auto">PhP {price}</Card.Text>


				        {
				        	user && !user.isAdmin?
				        		<Fragment>
				        		<Button  className="mt-auto btn-secondary shadow"  style={{ fontSize: '0.8rem' }} onClick={() => {
				        			setShowAddToCart(true); 
				        			setProductSearchId(`${_id}`)}
				        		}> Add to Cart </Button>
				        		<AddToCartModal prop={_id} show={showAddToCart} close={() => setShowAddToCart(false)}/>
				        		</Fragment>
				        	:

				        		user && user.isAdmin?
				        		<Button  className="btn-secondary shadow"  >Contact Admin</Button>
				        		:
				        		<Button  className="btn-secondary shadow" style={{ fontSize: '0.8rem' }} as = {Link} to ="/login">Login</Button>
				        
				        }
				    </Card.Body>
				</Card>
			</Container>
			</Col>
		</Row>

	</Fragment>
	)
}