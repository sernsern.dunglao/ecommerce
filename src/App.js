import './App.css';

import {useState, useEffect} from 'react';


import Register from './pages/Register.js';
import Home from './pages/Home.js';
import Logout from './pages/Logout.js';
import Login from './pages/Login.js';
import AddProducts from './pages/AddProducts.js';
import PageNotFound from './pages/PageNotFound.js';
import ProductsView from './pages/ProductsView.js'
import ProductSearch from './pages/ProductSearch.js'
import CartView from './pages/CartView.js'
import Orders from './pages/Orders.js'

import AppNavBar from './components/AppNavBar.js';
import UpdateItem from './components/UpdateItem.js'
import Archive from './components/Archive.js'
import AddToCartModal from './components/AddToCartModal.js'
import AddToCart from './components/AddToCart.js'
import ViewOrder from './components/ViewOrder.js'
import OrderCartItems from './components/OrderCartItems.js'







import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {UserProvider} from './UserContext.js';

function App() {

  const [user,setUser]= useState(null)

  // useEffect(()=>{
  //   console.log(user);
  // },[user])

  const unSetUser = () =>{
    localStorage.clear();
    
  }

  useEffect(()=>{

    document.title = "ebook"
   
    fetch(`${process.env.REACT_APP_API_URL}/user/account`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      if(localStorage.getItem('token') !== null){
        setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

      }else{
        setUser(null);
      }
    })
  },[])


  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path="/" element = {<Home/>}/>
          {/*<Route path="/courses" element = {<Courses/>}/>*/}
          <Route path="/login" element = {<Login/>}/>
          <Route path="/logout" element = {<Logout/>}/>
          <Route path="/register" element = {<Register/>}/>
          <Route path="*" element = {<PageNotFound/>}/>
          <Route path="/viewproducts" element = {<ProductsView/>}/>
          <Route path="/addProducts" element = {<AddProducts/>}/>
          <Route path="/updateitem/:productId" element = {<UpdateItem/>}/>
          <Route path="/archive/:productId/:status" element = {<Archive/>}/>
          <Route path="/productsearch" element = {<ProductSearch/>}/>
          <Route path="/addtocart/:productId" element = {<AddToCartModal/>}/>
          <Route path="/addtocart/:productId/:qty" element = {<AddToCart/>}/>
          <Route path="/cartview" element = {<CartView/>}/>
          <Route path="/orders" element = {<Orders/>}/>
          <Route path="/vieworder/:orderId" element = {<ViewOrder/>}/>
      

          {/*<Route path="/course/:courseId" element = {<CourseView/>}/>*/}
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
