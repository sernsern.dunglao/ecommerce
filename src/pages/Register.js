import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import {Fragment, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

// import the hooks that are needed in our page
import {useState, useEffect} from 'react';

export default function Register(){
	// Create 3 new states where we will store the cvalue from input of the email, password, and confirmPassword

	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	// const[user,setUser] = useState(localStorage.getItem('email'))

	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);

	// create another state for the button
	const [isActive, setIsActive] = useState(false);

	useEffect(() =>{
		
		if (email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword && firstName !== "" && lastName !== "" && mobileNo !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email,password,confirmPassword, firstName, lastName, mobileNo])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email: email,
				password : password,
				mobileNo : mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			if (data) {

				fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
					method: 'POST',
					headers:{
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email:email,
						password: password
					})
				}).then(result=>result.json())
				.then(data=>{
						localStorage.setItem('token', data.auth);
						retrieveUserDetails(localStorage.getItem('token'));

						Swal.fire({
					title: "Registered!",
					icon: "success",
					text: "You may now check our courses and offers."

				})
					setEmail('');
					setPassword('');
					setConfirmPassword('');
					navigate('/');
				})

				
			}
			else{
				Swal.fire({
					title: "Registration unsuccessful.",
					icon: "error",
					text: "Please check your email or contact our support team."

				})


			}



		})

		const retrieveUserDetails = (token) => {

		// The token sent as part of the request's header information

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}
	}

	return (
		user ?
		<Navigate to = "/*"/>
		:
		<Fragment>
				<Container className="">
				<h1 className ="text-center mt-5 text-muted"> SIGN UP </h1>
				<Form className = "my-5" onSubmit ={event =>register(event)} autoComplete="off">
					  
				     <Row className="">
				     <Form.Group as={Col} md="6" className="mb-2" controlId="validationFormik01">
				                   <Form.Label>First name</Form.Label>
				                   <Form.Control
				                     type="text"
				                     placeholder="First Name" 
				                     value={firstName}
				                     onChange={event=> setFirstName(event.target.value)}
				                     required
				                     autoComplete="off"
				                     className="shadow"
				                   />
				                 </Form.Group>

				                 <Form.Group as={Col} md="6" className='mb-2' controlId="validationFormik02">
				                   <Form.Label>Last name</Form.Label>
				                   <Form.Control
				                     placeholder="Last Name" 
				                     type="text"
				                     value={lastName}
				                     onChange={event=> setLastName(event.target.value)}
				                     required
				                     autoComplete="off"
				                     className="shadow"
				                   />
				  	  </Form.Group>
				  	  </Row>
				  	  <Row>
				      <Form.Group as={Col} md="6" className="mb-2" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value ={email} 
				        	onChange ={event=> setEmail(event.target.value)}
				        	required 
				        	autoComplete="new-email"
				        	className="shadow"/>
				        
				      </Form.Group>
				      <Form.Group as={Col} md="6" className="mb-2" controlId="validationFormik03">
				                   <Form.Label>Mobile Number</Form.Label>
				                   <Form.Control
				                     placeholder="+639-" 
				                     type="text"
				                     value={mobileNo}
				                     onChange={event=> setMobileNo(event.target.value)}
				                     required
				                     autoComplete="off"
				                     className="shadow"
				                   />
				                 </Form.Group>
				      </Row>
				      <Row>
				      <Form.Group as={Col} md="6" className="mb-2" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value ={password} 
				        	onChange ={event=> setPassword(event.target.value)}
				        	required
				        	autoComplete="new-password"
				        	className="shadow"/>
				      </Form.Group>

				      <Form.Group as={Col} md="6" className="mb-3" controlId="formBasicConfirmPassword">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value ={confirmPassword} 
				        	onChange ={event=> setConfirmPassword(event.target.value)}
				        	required
				        	autoComplete="new-password"
				        	className="shadow"/>
				      </Form.Group>
				      </Row>

				      <Row className=" ">
				      	<Col className="text-center">
						      {
						      	isActive ? 
						      	<Button variant="secondary" type="submit" className="shadow"> Submit </Button>
						    	:
						    	<Button className="mx-auto shadow" variant="light" type="submit" disabled> Submit </Button>
						      }
						</Col>
				      </Row>
				    
				</Form>
			</Container>
		</Fragment>

		)
}