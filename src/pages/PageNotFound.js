import {Container, Row , Col, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function PageNotFound() {
	
	return (
		<Container>
			<Row className="text-center mx-auto mt-5">
				<Col className="">
				<Image fluid="true" src =""/>
				<h1 className ="text center mt-1 text-muted"> Title </h1>
				
				<h4 className ="text-center mt-2 text-muted">Motto</h4>

				<h1 className ="text center mt-5 text-muted"> 404 Page Not Found. </h1>
				
				
				<h4 className ="text-center mt-2 text-muted">Go back to the <Link to ={"/"}>homepage</Link>.</h4>

				</Col>
					
			</Row>

		</Container>

		)

}