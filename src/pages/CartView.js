import {Button, Form, Row, Col, Table, Container} from 'react-bootstrap';
import {Fragment, useContext, useState, useEffect} from 'react';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

import {AiFillDelete} from  'react-icons/ai'
import {CgShoppingCart} from  'react-icons/cg'
import {IoMdDoneAll} from  'react-icons/io'

import '../index.css';

import CartItems from '../components/CartItems.js'


import UserContext from '../UserContext.js';


export default function ProductsView() {

  const[cartItems, setCartItems] = useState([]);
  const[totalAmount, setTotalAmount] = useState();
  const[completed, setCompleted] = useState(false);
  const {user} = useContext(UserContext);

  function display () {
    fetch(`${process.env.REACT_APP_API_URL}/order/reviewOrder/${localStorage.getItem('orderId')}/view`,{
      method: 'PUT',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result=>result.json())
    .then(data => {
    setTotalAmount(data.totalAmount) 
      // to change the value of our courses so we have to use the setCourses
      setCartItems(data.products.map(product=>{
        return (<CartItems key ={product._id} cartProp = {product}/>)
      }))
    })
  }


  useEffect(()=>{
    // fetch all products
    display()
  }, [])

  useEffect(()=>{
    // fetch all products
    fetch(`${process.env.REACT_APP_API_URL}/order/reviewOrder/${localStorage.getItem('orderId')}/view`,{
      method: 'PUT',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result=>result.json())
    .then(data => {
          
    })
  }, [])

  function CheckOut (){

    fetch(`${process.env.REACT_APP_API_URL}/order/reviewOrder/${localStorage.getItem('orderId')}/checkout`,{
      method: 'PUT',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result=>result.json())
    .then(data => {
        setTotalAmount(data.totalAmount)
        setCompleted(data.completed)
        fetch(`${process.env.REACT_APP_API_URL}/order/checkOut`,{
          method: 'POST',
          headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        }).then(result=>result.json())
        .then(data=>{
          localStorage.setItem('orderId', data._id);


        })
    })
  }

  function handleClick(event) {
    display();
  }

  useEffect(() => {
    
    window.addEventListener('click', handleClick);
  
    return () => window.removeEventListener('click', handleClick);
  }, []);

  return (
  
    user && !user.isAdmin? 

    <Fragment>  
      <Container className="mt-3">
            <h2><CgShoppingCart/></h2>
            <Table responsive="sm" striped bordered hover className="mt-2 shadow">
                 <thead>
                   <tr>
                     <th>Name</th>
                     <th>Description</th>
                     <th>Price</th>
                     <th>Qty</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                  {cartItems}
                 </tbody>
              </Table>
              <div className="text-center">
              <h6 className="pt-3 px-2">Total: PHP{totalAmount}</h6>

              {
                !completed && totalAmount?
                <Button onClick={()=>CheckOut()}variant="secondary shadow" type="submit"> Check Out </Button>
                :
                <Button variant="light shadow" type="submit" disabled> Completed </Button>
              }
              
              </div>
          
                
      </Container>
    </Fragment>

    
    :

    <Navigate to = "/productsearch"/>

    
  );
}
