import ProductCards from '../components/ProductCards.js'
import {Container,div, Card, Button, Form, Row, Col, InputGroup} from 'react-bootstrap';
import {Fragment, useEffect, useState} from 'react';
import {CgShoppingCart} from 'react-icons/cg'
import {NavLink} from 'react-router-dom'



export default function ProductSearch () {

	const[FrontProductSearch, setFrontProductSearch] = useState();
	const[search, setSearch] = useState();
	const[searchValue, setSearchValue] = useState();
	const[cart, setCart] = useState();

	const [productId, setProductId] = useState();
	
	function display (){
		fetch(`${process.env.REACT_APP_API_URL}/product/searchAll`)
		.then(result => result.json())
		.then(data => {
			setFrontProductSearch(data.map(products=>{
				return(<ProductCards key ={products._id} searchProductProp = {products}/>)
			}))
		})
		if (localStorage.getItem('token')){
			fetch(`${process.env.REACT_APP_API_URL}/order/checkOut`,{
				method: 'POST',
				headers:{
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(result=>result.json())
			.then(cart=>{
				setCart(cart.products.length);
			})
		}
		


	}



	useEffect(()=>{
	function display2 (){
		fetch(`${process.env.REACT_APP_API_URL}/product/searchAll`)
		.then(result => result.json())
		.then(data => {
			setFrontProductSearch(data.map(products=>{
				return(<ProductCards key ={products._id} searchProductProp = {products}/>)
			}))
		})
		if (localStorage.getItem('token')){
			fetch(`${process.env.REACT_APP_API_URL}/order/checkOut`,{
				method: 'POST',
				headers:{
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(result=>result.json())
			.then(cart=>{
				setCart(cart.products.length);
			})
		}
		


	}
	}, [])


	useEffect(()=>{
		if (search==""){
			display();
		}else{
			fetch(`${process.env.REACT_APP_API_URL}/product/searchOne/${search}`)
			.then(result => result.json())
			.then(data => {
				if (data){
					setFrontProductSearch(data.map(products=>{
					return(<ProductCards key ={products._id} searchProductProp = {products}/>)	
					}))
				}
					
			})
		}

		
	}, [search])

	function handleClick(event) {
		display();
	}

	useEffect(() => {
		
		window.addEventListener('click', handleClick);
	
		return () => window.removeEventListener('click', handleClick);
	}, []);


	return ( 
		<Fragment>
			<Container className="">
			<InputGroup 
				size="sm" 
				className="my-3 shadow"
				value = {searchValue}
				placeholder="Search"
				onChange ={event=> setSearch(event.target.value)}
				>
		        <InputGroup.Text id="inputGroup-sizing-sm">SEARCH</InputGroup.Text>
		        <Form.Control
		          aria-label="Small"
		          aria-describedby="inputGroup-sizing-sm"
		        />
		        {
		        	cart ?
		        	<InputGroup.Text as={NavLink} to = "/cartview" id="inputGroup-sizing-sm"><h5 className="d-flex align-items-center justify-content-center">{cart}<CgShoppingCart/></h5></InputGroup.Text>
		        	:
		        	<InputGroup.Text id="inputGroup-sizing-sm" className="bg-light"><h5 className="d-flex align-items-center justify-content-center">{cart}<CgShoppingCart/></h5></InputGroup.Text>	


		        }
		       
		      </InputGroup>
			</Container>
			<div className="col-10 mt-2 d-flex flex-wrap justify-content-center offset-1 mx-auto">
						{FrontProductSearch}
			</div>
		</Fragment>
		)
}

