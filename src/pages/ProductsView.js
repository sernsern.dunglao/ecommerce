import { Table, Container} from 'react-bootstrap';
import {Fragment, useContext, useState, useEffect} from 'react';
import {Navigate} from 'react-router-dom';

import '../index.css';

import Products from '../components/Products.js'
import UserContext from '../UserContext.js';


export default function ProductsView() {

  const {user} = useContext(UserContext);



  const[products, setProducts] = useState([]);

  useEffect(()=>{
    // fetch all products
    fetch(`${process.env.REACT_APP_API_URL}/product/searchAllProduct`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
  
      // to change the value of our courses so we have to use the setCourses
      setProducts(data.map(product=>{
        return (<Products key ={product._id} productsProp = {product}/>)
      }))
    })
  }, [])





  return (
  
    user && user.isAdmin? 

    <Fragment>  
      <Container className="">
            <Table responsive="sm" striped bordered hover className="mt-5 shadow">
                 <thead>
                   <tr>
                     <th>ID</th>
                     <th>Name</th>
                     <th>Description</th>
                     <th>Price</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                    {products}
                 </tbody>
              </Table>
      </Container>
    </Fragment>

    
    :

    <Navigate to = "/"/>

    
  );
}
